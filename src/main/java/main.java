import com.github.javafaker.*;

import java.util.ArrayList;

public class main {
    public static void main(String[] args) {
        Faker faker = new Faker();

        ArrayList<Integer> weatherTemps = new ArrayList<Integer>();

        int totalTemperatures = 50;
        for (int i = 0; i < totalTemperatures; i++) {
            weatherTemps.add(faker.number().numberBetween(0, 10000));
        }
        ArrayList<Integer> fakeNumArray = fakeNumbers(0, 10000, 100);
        int lastNumber = 0;
        int nextNumber = 0;

        for (int i = 0; i < fakeNumArray.size(); i++) {
            lastNumber = fakeNumArray.get(i);
            System.out.println(lastNumber);
            nextNumber = fakeNumArray.get(i+1);
            System.out.println(nextNumber);
            if (lastNumber > nextNumber) {
                fakeNumArray.add(i + 1, lastNumber);
                fakeNumArray.add(i, nextNumber);
                System.out.println("Bigger");

            } else {
                System.out.println("Smaller");
            }



        }



    }

    /**
     *
     * @param min Min
     * @param max Max
     * @param maxAmount Amount of Random Numbers
     * @return Array of Random Integers
     */
    private static ArrayList<Integer> fakeNumbers(int min, int max, int maxAmount) {
        Faker faker = new Faker();
        int realMax = (maxAmount - 1);


        ArrayList<Integer> arrayToReturn = new ArrayList<Integer>();

        for (int i = 0; i < maxAmount; i++) {
            arrayToReturn.add(faker.number().numberBetween(min, max));

        }
        return arrayToReturn;


}
}

